package vandroux.aurore.ptit_quizz.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

import vandroux.aurore.ptit_quizz.DataRepository;
import vandroux.aurore.ptit_quizz.db.entity.QuizzEntity;

public class QuizzListViewModel extends AndroidViewModel {

    private final DataRepository mRepository;

    // MediatorLiveData can observe other LiveData objects and react on their emissions.
    private final LiveData<List<QuizzEntity>> mObservableQuizzs;

    public QuizzListViewModel(Application application) {
        super(application);

        mRepository = new DataRepository(application);

        mObservableQuizzs = mRepository.getQuizzs();
    }

   /*
    *------------------------
    *       FUNCTIONS
    *------------------------
    */

    public LiveData<List<QuizzEntity>> getQuizzs() {
        return mObservableQuizzs;
    }

    public void insert (QuizzEntity quizz){
        mRepository.insertQuizz(quizz);
    }

}
