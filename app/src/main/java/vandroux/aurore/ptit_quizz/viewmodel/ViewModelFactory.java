package vandroux.aurore.ptit_quizz.viewmodel;

import android.app.Application;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import vandroux.aurore.ptit_quizz.viewmodel.QuestionListViewModel;

public class ViewModelFactory extends ViewModelProvider.NewInstanceFactory{

    private Application mApplication;
    private Object[] mParams;

    public ViewModelFactory(Application application, Object... params) {
        mApplication = application;
        mParams = params;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass == QuestionListViewModel.class) {
            return (T) new QuestionListViewModel(mApplication, (Integer) mParams[0]);
        }
        else if (modelClass == QuestionViewModel.class) {
            return (T) new QuestionViewModel(mApplication, (Integer) mParams[0]);
        }
        else {
            return super.create(modelClass);
        }
    }


}
