package vandroux.aurore.ptit_quizz.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

import vandroux.aurore.ptit_quizz.DataRepository;
import vandroux.aurore.ptit_quizz.db.entity.QuestionEntity;
import vandroux.aurore.ptit_quizz.db.entity.QuizzEntity;


public class QuestionListViewModel extends AndroidViewModel {
    private final DataRepository mRepository;

    private final int quizzId;

    // MediatorLiveData can observe other LiveData objects and react on their emissions.
    private final LiveData<List<QuestionEntity>> mObservableQuestions;

    public QuestionListViewModel(Application application, int quizzID) {
        super(application);

        this.quizzId = quizzID;

        mRepository = new DataRepository(application);

        mObservableQuestions = mRepository.getQuestionsOfQuizz(this.quizzId);
    }

    /*
     *------------------------
     *       FUNCTIONS
     *------------------------
     */

    public LiveData<List<QuestionEntity>> getQuestions() {
        return mObservableQuestions;
    }

    public List<QuestionEntity> getQuestionsSync(){
        return mRepository.getQuestionsOfQuizzSync(this.quizzId);
    }

    public QuizzEntity getQuizzSync(){
        return mRepository.getQuizzSync(quizzId);
    }

    public void deleteQuestion(int questionId){
        mRepository.deleteQuestion(questionId);
    }

    public void deleteQuizz(int quizzId){
        mRepository.deleteQuizz(quizzId);
    }

    public void updateQuizz(QuizzEntity quizz){
        mRepository.updateQuizz(quizz);
    }
}
