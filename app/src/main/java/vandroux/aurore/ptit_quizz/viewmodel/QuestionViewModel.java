package vandroux.aurore.ptit_quizz.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

import vandroux.aurore.ptit_quizz.DataRepository;
import vandroux.aurore.ptit_quizz.db.entity.AnswerEntity;
import vandroux.aurore.ptit_quizz.db.entity.QuestionEntity;

public class QuestionViewModel extends AndroidViewModel {

    private final DataRepository mRepository;

    // MediatorLiveData can observe other LiveData objects and react on their emissions.
    private final LiveData<QuestionEntity> mObservableQuestion;
    private final LiveData<List<AnswerEntity>> mObservableAnswers ;

    private final int questionId ;

    public QuestionViewModel(Application application, int questionID) {
        super(application);

        this.questionId = questionID;

        mRepository = new DataRepository(application);

        mObservableQuestion = mRepository.getQuestion(questionID);
        mObservableAnswers = mRepository.getAnswersOfQuestion(questionID);

    }

    /*
     *------------------------
     *       FUNCTIONS
     *------------------------
     */


    //      GET
    public LiveData<QuestionEntity> getQuestion() {
        return mObservableQuestion;
    }

    public QuestionEntity getQuestionSync() { return mRepository.getQuestionSync(questionId); }

    public LiveData<List<AnswerEntity>> getAnswers() { return mObservableAnswers; }

    public List<AnswerEntity> getAnswersSync() {
        return mRepository.getAnswersOfQuestionSync(questionId);
    }

    //      INSERT
    public long insert (QuestionEntity question){
        return mRepository.insertQuestion(question);
    }

    public void insert (AnswerEntity answer){
        mRepository.insertAnswer(answer);
    }


    //      UPDATE
    public void update(QuestionEntity question){
        mRepository.updateQuestion(question);
    }

    //      DELETE
    public void deleteAllAnswers(){
        mRepository.deleteAnswersOfQuestion(questionId);
    };

}
