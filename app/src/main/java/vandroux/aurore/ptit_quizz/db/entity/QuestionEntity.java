package vandroux.aurore.ptit_quizz.db.entity;

//import androidx.room.ColumnInfo;
//import androidx.room.Entity;
//import androidx.room.ForeignKey;
//import androidx.room.Ignore;
//import androidx.room.Index;
//import androidx.room.PrimaryKey;
//import static androidx.room.ForeignKey.CASCADE;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import vandroux.aurore.ptit_quizz.model.Question;

import static android.arch.persistence.room.ForeignKey.CASCADE;


@Entity(tableName = "questions",
        foreignKeys = {
            @ForeignKey(
                entity = QuizzEntity.class,
                parentColumns = "id",
                childColumns = "quizz_id",
                onDelete = CASCADE)},
        indices = {@Index(value = "quizz_id")
        })
public class QuestionEntity implements Question {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "quizz_id")
    private int quizzId;

    private String question;

    @ColumnInfo(name = "good_answer")
    private String goodAnswer;

    @Override
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int getQuizzId() {
        return quizzId;
    }

    public void setQuizzId(int quizzId) {
        this.quizzId = quizzId;
    }

    @Override
    public String getQuestion () {
        return this.question;
    }

    public void setQuestion (String question){
        this.question = question;
    }

    @Override
    public String getGoodAnswer () {
        return this.goodAnswer;
    }

    public void setGoodAnswer (String goodAnswer){
        this.goodAnswer = goodAnswer;
    }

    public QuestionEntity(){

    }

    @Ignore
    public QuestionEntity (int id, String question,String goodAnswer, int quizzId){
        this.question = question;
        this.goodAnswer = goodAnswer;
        this.quizzId = quizzId;
        this.id = id;
    }

    public QuestionEntity (Question question){
        this.question = question.getQuestion();
        this.goodAnswer =question.getGoodAnswer();
        this.quizzId = question.getQuizzId();
        this.id = question.getId();
    }
}
