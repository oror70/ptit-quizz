package vandroux.aurore.ptit_quizz.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

//import androidx.lifecycle.LiveData;
//import androidx.room.Dao;
//import androidx.room.Delete;
//import androidx.room.Insert;
//import androidx.room.OnConflictStrategy;
//import androidx.room.Query;
//import androidx.room.Update;

import vandroux.aurore.ptit_quizz.db.entity.AnswerEntity;

@Dao
public interface AnswerDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(AnswerEntity answer);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<AnswerEntity> answers);

    @Update
    void update(AnswerEntity answer);

    @Delete
    void delete(AnswerEntity answer);

    @Query("DELETE FROM answers WHERE question_id  = :questionId")
    void deleteAnswersOfQuestion(int questionId);

    @Query("SELECT * FROM answers WHERE question_id = :questionId")
    LiveData<List<AnswerEntity>> loadAnswersOfQuestion(int questionId);

    @Query("SELECT * FROM answers WHERE question_id = :questionId")
    List<AnswerEntity> loadAnswersOfQuestionSync(int questionId);


}
