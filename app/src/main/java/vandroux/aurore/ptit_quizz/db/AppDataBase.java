package vandroux.aurore.ptit_quizz.db;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;

import java.util.List;

//import androidx.annotation.NonNull;
//import androidx.annotation.VisibleForTesting;
//import androidx.lifecycle.LiveData;
//import androidx.lifecycle.MutableLiveData;
//import androidx.room.Database;
//import androidx.room.Room;
//import androidx.room.RoomDatabase;
//import androidx.sqlite.db.SupportSQLiteDatabase;
//import androidx.annotation.VisibleForTesting;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

import vandroux.aurore.ptit_quizz.db.dao.AnswerDao;
import vandroux.aurore.ptit_quizz.db.dao.QuestionDao;
import vandroux.aurore.ptit_quizz.db.dao.QuizzDao;
import vandroux.aurore.ptit_quizz.db.entity.AnswerEntity;
import vandroux.aurore.ptit_quizz.db.entity.QuestionEntity;
import vandroux.aurore.ptit_quizz.db.entity.QuizzEntity;


@Database(entities = {QuizzEntity.class, QuestionEntity.class, AnswerEntity.class}, version = 1)
public abstract class AppDataBase extends RoomDatabase {

    @VisibleForTesting
    public static final String DATABASE_NAME = "ptitquizz-database";

    public abstract QuizzDao quizzDao();
    public abstract QuestionDao questionDao();
    public abstract AnswerDao answerDao();

    private final MutableLiveData<Boolean> IsDatabaseCreated = new MutableLiveData<>();


    //make AppDataBase a singleton to prevent having multiple instances of the database at the same time
    private static AppDataBase Instance;

    public static AppDataBase getInstance(final Context context){
        if(Instance == null){
            synchronized (AppDataBase.class){
                if(Instance == null){
                    //create data base here
                    Instance = buildDatabase(context.getApplicationContext());
                    Instance.updateDatabaseCreated(context.getApplicationContext());
                }
            }
        }
        return Instance;
    }


    /**
     * Build the database. {@link Builder#build()} only sets up the database configuration and
     * creates a new instance of the database.
     * The SQLite database is only created when it's accessed for the first time.
     */
    private static AppDataBase buildDatabase(final Context appContext) {
        return Room.databaseBuilder(appContext, AppDataBase.class, DATABASE_NAME)
                .addCallback(new Callback() {
                    @Override
                    public void onCreate(@NonNull SupportSQLiteDatabase db) {
                        super.onCreate(db);
                        //populate the database
                        new PopulateDbAsync(Instance).execute();

                        // notify that the database was created and it's ready to be used
                        Instance.setDatabaseCreated();
                    }
                })
                .allowMainThreadQueries()
                .build();
    }

    //Populate the database in the background.
    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final QuizzDao mQuizzDao;
        private final QuestionDao mQuestionDao;
        private final AnswerDao mAnswerDao;

        PopulateDbAsync(AppDataBase db) {
            mQuizzDao = db.quizzDao();
            mQuestionDao = db.questionDao();
            mAnswerDao = db.answerDao();
        }

        @Override
        protected Void doInBackground(final Void... params) {

            // Generate the data for pre-population
            List<QuizzEntity> quizzs = DataGenerator.generateQuizzs();
            List<QuestionEntity> questions = DataGenerator.generateQuestions();
            List<AnswerEntity> answers = DataGenerator.generateAnswers();


            mQuizzDao.insertAll(quizzs);
            mQuestionDao.insertAll(questions);
            mAnswerDao.insertAll(answers);

            return null;
        }
    }


    /**
     * Check whether the database already exists and expose it via {@link #getDatabaseCreated()}
     */
    private void updateDatabaseCreated(final Context context) {
        if (context.getDatabasePath(DATABASE_NAME).exists()) {
            setDatabaseCreated();
        }
    }

    private void setDatabaseCreated(){
        IsDatabaseCreated.postValue(true);
    }

    public LiveData<Boolean> getDatabaseCreated() {
        return IsDatabaseCreated;
    }
}
