package vandroux.aurore.ptit_quizz.db;

import java.util.ArrayList;
import java.util.List;

import vandroux.aurore.ptit_quizz.db.entity.AnswerEntity;
import vandroux.aurore.ptit_quizz.db.entity.QuestionEntity;
import vandroux.aurore.ptit_quizz.db.entity.QuizzEntity;


/**
 * Generates data to pre-populate the database
 */
public class DataGenerator {

    private static final String[] QUIZZS_NAMES = new String[]{
            "Quizz 1", "Quizz 2"};

    private static final String[][] QUESTIONS_QUESTIONS = new String[][]{
            {"question 1 Q1", "Questions2 Q1"},
            {"question 1 Q2", "question 2 Q2", "question 3 Q2" } };

    private static final String[][] QUESTIONS_GOOD_ANSWERS = new String[][]{
            {"true", "answer3"},
            {"hahaha", "false", "green" } };

    private static final String[][][] QUESTIONS_WRONG_ANSWERS = new String[][][]{
            {{"false"}, { "answer1", "answer2", "answer4"}},
            {{"tranquille", "bonjours", "bienvenue"}, {"true"}, {"blue", "yellow"} } };



    public static List<QuizzEntity> generateQuizzs() {
        List<QuizzEntity> quizzs = new ArrayList<>(QUIZZS_NAMES.length);

        for (int i = 0; i < QUIZZS_NAMES.length; i++) {

            QuizzEntity quizz = new QuizzEntity();
            quizz.setName(QUIZZS_NAMES[i]);
            quizz.setId(i+1);

            quizzs.add(quizz);
        }

        return quizzs;
    }

    public static List<QuestionEntity> generateQuestions() {
        List<QuestionEntity> questions = new ArrayList<>();

        int questionNumber = 0;

        for (int i = 0; i < QUIZZS_NAMES.length; i++) {
            for (int j = 0; j<QUESTIONS_QUESTIONS[i].length ; j++){

                QuestionEntity question = new QuestionEntity();

                question.setQuestion(QUESTIONS_QUESTIONS[i][j]);
                question.setGoodAnswer(QUESTIONS_GOOD_ANSWERS[i][j]);
                question.setQuizzId(i+1);

                questionNumber++;
                question.setId(questionNumber);

                questions.add(question);
            }
        }

        return questions;
    }

    public static List<AnswerEntity> generateAnswers() {
        List<AnswerEntity> answers = new ArrayList<>();

        int questionNumber = 0;
        int answerNumber = 0;

        for (int i = 0; i < QUIZZS_NAMES.length; i++) {
            for (int j = 0; j<QUESTIONS_QUESTIONS[i].length ; j++){

                questionNumber++;

                for(int k = 0; k<QUESTIONS_WRONG_ANSWERS[i][j].length; k++){
                    AnswerEntity answer = new AnswerEntity();

                    answerNumber++;
                    answer.setId(answerNumber);
                    answer.setQuestionId(questionNumber);
                    answer.setText(QUESTIONS_WRONG_ANSWERS[i][j][k]);

                    answers.add(answer);
                }
            }
        }

        return answers;
    }
}
