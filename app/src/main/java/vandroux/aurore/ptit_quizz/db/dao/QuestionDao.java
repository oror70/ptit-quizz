package vandroux.aurore.ptit_quizz.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;


//import androidx.lifecycle.LiveData;
//import androidx.room.Dao;
//import androidx.room.Delete;
//import androidx.room.Insert;
//import androidx.room.OnConflictStrategy;
//import androidx.room.Query;
//import androidx.room.Update;
import vandroux.aurore.ptit_quizz.db.entity.QuestionEntity;


@Dao
public interface QuestionDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(QuestionEntity question);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<QuestionEntity> questions);

    @Update
    void update(QuestionEntity question);

    @Delete
    void delete(QuestionEntity question);

    //-----------------------

    @Query("SELECT * FROM questions")
    LiveData<List<QuestionEntity>> loadAllQuestions();

    @Query("SELECT * FROM questions")
    List<QuestionEntity> loadAllQuestionsSync();

    //--------------------

    @Query("SELECT * FROM questions WHERE quizz_id = :quizzId")
    LiveData<List<QuestionEntity>> loadQuestionsOfQuizz(int quizzId);

    @Query("SELECT * FROM questions WHERE quizz_id = :quizzId")
    List<QuestionEntity> loadQuestionsOfQuizzSync(int quizzId);

    //-------------------------

    @Query("SELECT * FROM questions WHERE id = :questionId")
    LiveData<QuestionEntity> loadQuestion(int questionId);

    @Query("SELECT * FROM questions WHERE id = :questionId")
    QuestionEntity loadQuestionSync(int questionId);

    //----------------------

    @Query("DELETE FROM questions WHERE id = :questionId")
    void deleteQuestion(int questionId);

}
