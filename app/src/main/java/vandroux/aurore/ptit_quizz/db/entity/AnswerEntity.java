package vandroux.aurore.ptit_quizz.db.entity;

//import androidx.room.ColumnInfo;
//import androidx.room.Entity;
//import androidx.room.ForeignKey;
//import androidx.room.Ignore;
//import androidx.room.Index;
//import androidx.room.PrimaryKey;
//import static androidx.room.ForeignKey.CASCADE;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import vandroux.aurore.ptit_quizz.model.Answer;

import static android.arch.persistence.room.ForeignKey.CASCADE;


@Entity(tableName = "answers",
        foreignKeys = {
                @ForeignKey(
                        entity = QuestionEntity.class,
                        parentColumns = "id",
                        childColumns = "question_id",
                        onDelete = CASCADE)},
        indices = {@Index(value = "question_id")
        })
public class AnswerEntity implements Answer {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private String text ;

    @ColumnInfo(name = "question_id")
    private int questionId;

    @Override
    public int getId() {
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    @Override
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId){
        this.questionId = questionId;
    }

    public AnswerEntity () {

    }

    @Ignore
    public AnswerEntity (int id,String text,int questionId){
        this.text = text;
        this.questionId = questionId;
        this.id =id;
    }

    public AnswerEntity (Answer answer){
        this.id = answer.getId();
        this.questionId = answer.getQuestionId();
        this.text = answer.getText();
    }

}
