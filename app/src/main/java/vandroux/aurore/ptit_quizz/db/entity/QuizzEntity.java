package vandroux.aurore.ptit_quizz.db.entity;

//import androidx.room.Entity;
//import androidx.room.Ignore;
//import androidx.room.PrimaryKey;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import vandroux.aurore.ptit_quizz.model.Quizz;

@Entity(tableName = "quizzs")
public class QuizzEntity implements Quizz {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private String name;

    @Override
    public int getId() {
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public QuizzEntity(){

    }

    @Ignore
    public QuizzEntity (int id, String name){
        this.name = name;
        this.id = id;
    }

    public QuizzEntity(Quizz quizz){
        this.id = quizz.getId();
        this.name = quizz.getName();
    }
}
