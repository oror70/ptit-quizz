package vandroux.aurore.ptit_quizz.db.dao;


import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

//import androidx.lifecycle.LiveData;
//import androidx.room.Dao;
//import androidx.room.Delete;
//import androidx.room.Insert;
//import androidx.room.OnConflictStrategy;
//import androidx.room.Query;
//import androidx.room.Update;
import vandroux.aurore.ptit_quizz.db.entity.QuizzEntity;

@Dao
public interface QuizzDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(QuizzEntity quizz);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<QuizzEntity> quizzs);

    @Update
    void update(QuizzEntity quizz);

    @Delete
    void delete(QuizzEntity quizz);

    //-----------------

    @Query("SELECT * FROM quizzs")
    LiveData<List<QuizzEntity>> loadAllQuizzs();

    @Query("SELECT * FROM quizzs")
    List<QuizzEntity> loadAllQuizzsSync();

    //-------------------------

    @Query("SELECT * FROM quizzs WHERE id = :quizzId")
    LiveData<QuizzEntity> loadQuizz(int quizzId);

    @Query("SELECT * FROM quizzs WHERE id = :quizzId")
    QuizzEntity loadQuizzSync(int quizzId);

    //------------------

    @Query("DELETE FROM quizzs WHERE id = :quizzId")
    void deleteQuizz (int quizzId);


}
