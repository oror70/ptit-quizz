package vandroux.aurore.ptit_quizz.model;

public interface Answer {
    int getId();
    int getQuestionId();
    String getText();
}
