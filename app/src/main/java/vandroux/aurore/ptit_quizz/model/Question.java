package vandroux.aurore.ptit_quizz.model;


public interface Question {
    int getId();
    int getQuizzId();
    String getQuestion();
    String getGoodAnswer();
}

