package vandroux.aurore.ptit_quizz.model;

public interface Quizz {
    String getName();
    int getId();
}
