//package vandroux.aurore.ptit_quizz;
//
//import android.app.Application;
//
//import vandroux.aurore.ptit_quizz.db.AppDataBase;
//
///**
// * Android Application class. Used for accessing singletons.
// */
//public class BasicApp extends Application {
//
//    private AppExecutors mAppExecutors;
//
//    @Override
//    public void onCreate() {
//        super.onCreate();
//
//        mAppExecutors = new AppExecutors();
//    }
//
//    public AppDataBase getDatabase() {
//        return AppDataBase.getInstance(this, mAppExecutors);
//    }
//
//    public DataRepository getRepository() {
//        return DataRepository.getInstance(getDatabase());
//    }
//}
