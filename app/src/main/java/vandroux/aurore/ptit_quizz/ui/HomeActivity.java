package vandroux.aurore.ptit_quizz.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import vandroux.aurore.ptit_quizz.R;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }

    /** Called when the user taps the PLAY button */
    public void goToSelectQuizz(View view) {
        Intent intent = new Intent(this, SelectQuizzActivity.class);
        startActivity(intent);
    }

    /** Called when the user taps the MANAGE QUIZZ button */
    public void goToManageQuizz(View view) {
        Intent intent = new Intent(this, ManageQuizzActivity.class);
        startActivity(intent);
    }
}
