package vandroux.aurore.ptit_quizz.ui;

public interface GoToNextActivityInterface {
    void goToNextActivity (Object ... params);
}
