package vandroux.aurore.ptit_quizz.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import vandroux.aurore.ptit_quizz.R;
import vandroux.aurore.ptit_quizz.db.entity.AnswerEntity;

public class WrongAnswersAdapter extends RecyclerView.Adapter<WrongAnswersAdapter.WrongAnswersViewHolder>{

    class WrongAnswersViewHolder extends RecyclerView.ViewHolder {
        private final EditText WrongAnswerItemView;
        private final ImageButton DeleteButtonItemView;
        private TextWatcher WrongAnswerTextWatcher ;

        private WrongAnswersViewHolder(View itemView) {
            super(itemView);
            WrongAnswerItemView = itemView.findViewById(R.id.wrong_answer_EditText);
            DeleteButtonItemView = itemView.findViewById(R.id.wrong_answer_delete_button);
            WrongAnswerTextWatcher = null;
        }
    }

    private final LayoutInflater mInflater;
    private List<AnswerEntity> mAnswers = Collections.emptyList(); // Cached copy of answers
    private GoToNextActivityInterface goToNextActivityInterface;


    private Timer timer = new Timer();
    private final long DELAY = 400;


    WrongAnswersAdapter (Context context) {
        mInflater = LayoutInflater.from(context);
    }


    @Override
    public WrongAnswersAdapter.WrongAnswersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.wrong_anser_recyclerview_item, parent, false);

        return new WrongAnswersAdapter.WrongAnswersViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(WrongAnswersAdapter.WrongAnswersViewHolder holder, int position) {

        AnswerEntity current = mAnswers.get(position);
        holder.WrongAnswerItemView.setText(current.getText());

        if(holder.WrongAnswerTextWatcher != null){
            holder.WrongAnswerItemView.removeTextChangedListener(holder.WrongAnswerTextWatcher);
        }

        holder.WrongAnswerTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(timer != null)
                    timer.cancel();
            }

            @Override
            public void afterTextChanged(Editable s) {
                //avoid triggering event when text is too short
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        // (refresh list)
                        current.setText(s.toString());
                    }

                }, DELAY);

            }
        };

        holder.WrongAnswerItemView.addTextChangedListener(holder.WrongAnswerTextWatcher);



        holder.DeleteButtonItemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeAnswer(current);
            }
        });

    }

    void removeAnswer(AnswerEntity answer){
        mAnswers.remove(answer);
        notifyDataSetChanged();
    }

    void addAnswer(AnswerEntity answer){
        mAnswers.add(answer);
        notifyDataSetChanged();
    }

    void setAnswers(List<AnswerEntity> answers) {
        mAnswers = answers;
        notifyDataSetChanged();
    }

    List<AnswerEntity> getAnswers(){
        return mAnswers;
    }


    @Override
    public int getItemCount() {
        return mAnswers.size();
    }
}
