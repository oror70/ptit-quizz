package vandroux.aurore.ptit_quizz.ui;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;


import java.util.List;

import vandroux.aurore.ptit_quizz.R;
import vandroux.aurore.ptit_quizz.db.entity.QuestionEntity;
import vandroux.aurore.ptit_quizz.db.entity.QuizzEntity;
import vandroux.aurore.ptit_quizz.viewmodel.QuestionListViewModel;
import vandroux.aurore.ptit_quizz.viewmodel.ViewModelFactory;

import static android.text.InputType.TYPE_CLASS_TEXT;

public class ManageQuestionsActivity extends AppCompatActivity  implements GoToNextActivityInterface, DeleteQuestionInterface{

    public static final String QUESTION_ID_MESSAGE = "vandroux.aurore.QUESTION_ID";
    public static final String QUIZZ_ID_MESSAGE = "vandroux.aurore.QUIZZ_ID";
    public static final String CONFIGURE_MODE = "vandroux.aurore.CONFIGURE_MODE";

    private QuestionListViewModel mQuestionListViewModel;

    private int quizzID ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_questions);

        // Get the Intent that started this activity and extract the quizzID
        Intent intent = getIntent();

        this.quizzID =  intent.getIntExtra(ManageQuizzActivity.QUIZZ_ID_MESSAGE, 0);

        //set Adapter
        final QuestionListAdapter adapter = new QuestionListAdapter(this);
        RecyclerView recyclerView = findViewById(R.id.recyclerview_manage_questions);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //set view model
        mQuestionListViewModel = ViewModelProviders.of(this,
                new ViewModelFactory(getApplication(), this.quizzID)).get(QuestionListViewModel.class);

        // Add an observer on the LiveData returned by getQuizzs().
        // The onChanged() method fires when the observed data changes and the activity is
        // in the foreground.
        mQuestionListViewModel.getQuestions().observe(this, new Observer<List<QuestionEntity>>() {
            @Override
            public void onChanged(@Nullable final List<QuestionEntity> questions) {
                // Update the cached copy of the quizzs in the adapter.
                adapter.setQuestions(questions);
            }
        });

        //set the title of the activity (the quizz name)
        setHeaders(this);


        Toast.makeText(getApplicationContext(), "quizz ID : "+quizzID, Toast.LENGTH_SHORT).show();

        FloatingActionButton fab = findViewById(R.id.fab_manage_questions);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "nouvelle question", Toast.LENGTH_SHORT).show();
                goToNextActivity(-1, true);
            }
        });
    }

    public void goToConfigureQuestionActivityOfQuizz(int questionId, boolean newQuestion){
        Intent intent = new Intent(ManageQuestionsActivity.this, ConfigureQuestionActivity.class);
        intent.putExtra(QUESTION_ID_MESSAGE, questionId);
        intent.putExtra(QUIZZ_ID_MESSAGE, quizzID);
        intent.putExtra(CONFIGURE_MODE, newQuestion);
        startActivity(intent);
    }

    public void goToNextActivity(Object ... params){
        goToConfigureQuestionActivityOfQuizz((int) params[0], (boolean) params[1]);
    }

    public void deleteQuestion(int questionId){
        mQuestionListViewModel.deleteQuestion(questionId);
    }

    public void deleteQuizz(int quizzId){
        mQuestionListViewModel.deleteQuizz(quizzId);
    }


    private void setHeaders(Context context){
        QuizzEntity quizz = mQuestionListViewModel.getQuizzSync();

        //set quizz name
        TextView quizzNameTitle = findViewById(R.id.manage_questions_title);
        quizzNameTitle.setText(quizz.getName());

        //set the menu button
        ImageButton quizzMenuButton = findViewById(R.id.quizz_menuButton);
        quizzMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //display a popup menu
                PopupMenu popup = new PopupMenu(context, view);

                // This activity implements OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.rename_quizz_menu_item:

                                EditText EditQuizzNameView = new EditText(context);
                                EditQuizzNameView.setMaxLines(1);
                                EditQuizzNameView.setInputType(TYPE_CLASS_TEXT);
                                EditQuizzNameView.setFilters(new InputFilter[]{new InputFilter.LengthFilter(30)});

                                //display an alert dialog to set the new name for the quizz
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                                alertDialogBuilder.setView(EditQuizzNameView);
                                alertDialogBuilder.setMessage(R.string.update_name_for_quizz_dialog);
                                alertDialogBuilder.setPositiveButton(R.string.update, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        //check if the input is correct
                                        if (TextUtils.isEmpty(EditQuizzNameView.getText())) {
                                            Toast.makeText(getApplicationContext(), R.string.empty_quizz_name, Toast.LENGTH_SHORT).show();
                                        } else {
                                            String quizzName = EditQuizzNameView.getText().toString();

                                            //update database
                                            QuizzEntity quizz = new QuizzEntity();
                                            quizz.setName(quizzName);
                                            quizz.setId(quizzID);

                                            mQuestionListViewModel.updateQuizz(quizz);


                                            //udpate the view
                                            quizzNameTitle.setText(quizzName);
                                        }
                                    }
                                } );
                                alertDialogBuilder.setNegativeButton(R.string.cancel, null);
                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                                return true;


                            case R.id.delete_quizz_menu_item:
                                //display an alert dialog to ask the user if he really want to delete the quizz
                                AlertDialog.Builder alertDialogBuilder2 = new AlertDialog.Builder(context);
                                alertDialogBuilder2.setMessage(R.string.dialog_delete_quizz);
                                alertDialogBuilder2.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //delete question from database
                                        deleteQuizz(quizzID);

                                        //finish activity
                                        finish();
                                    }
                                } );
                                alertDialogBuilder2.setNegativeButton(R.string.cancel, null);
                                AlertDialog alertDialog2 = alertDialogBuilder2.create();
                                alertDialog2.show();

                                return true;

                            default:
                                return false;
                        }
                    }});

                popup.inflate(R.menu.quizz_menu_item);
                popup.show();
            }
        });
    }
}
