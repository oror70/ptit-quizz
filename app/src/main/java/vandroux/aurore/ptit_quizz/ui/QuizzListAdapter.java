package vandroux.aurore.ptit_quizz.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import vandroux.aurore.ptit_quizz.R;

import vandroux.aurore.ptit_quizz.db.entity.QuizzEntity;

public class QuizzListAdapter extends RecyclerView.Adapter<QuizzListAdapter.QuizzViewHolder> {


    class QuizzViewHolder extends RecyclerView.ViewHolder {
        private final TextView QuizzItemTextView;
        private final LinearLayout QuizzItemView;

        private QuizzViewHolder(View itemView) {
            super(itemView);
            QuizzItemTextView = itemView.findViewById(R.id.quizz_name_textView);
            QuizzItemView = itemView.findViewById(R.id.quizz_name_box);
        }
    }

    private final LayoutInflater mInflater;
    private List<QuizzEntity> mQuizzs = Collections.emptyList(); // Cached copy of words
    private GoToNextActivityInterface goToNextActivityInterface;



    QuizzListAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
        try {
            this.goToNextActivityInterface = ((GoToNextActivityInterface) context);
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement GoToNextActivityInterface.");
        }
    }


    @Override
    public QuizzViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.quizz_list_recyclerview_item, parent, false);
        return new QuizzViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(QuizzViewHolder holder, int position) {
        QuizzEntity current = mQuizzs.get(position);
        holder.QuizzItemTextView.setText(current.getName());
        holder.QuizzItemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToNextActivityInterface.goToNextActivity(current.getId());
            }
        });
    }

    void setQuizzs(List<QuizzEntity> quizzs) {
        mQuizzs = quizzs;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mQuizzs.size();
    }

}
