package vandroux.aurore.ptit_quizz.ui;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.List;

import vandroux.aurore.ptit_quizz.R;
import vandroux.aurore.ptit_quizz.db.entity.AnswerEntity;
import vandroux.aurore.ptit_quizz.db.entity.QuestionEntity;
import vandroux.aurore.ptit_quizz.viewmodel.QuestionViewModel;
import vandroux.aurore.ptit_quizz.viewmodel.ViewModelFactory;

public class ConfigureQuestionActivity extends AppCompatActivity {

    private int questionID ;
    private int quizzID ;
    private boolean newQuestion ;

    private QuestionViewModel mQuestionViewModel ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configure_question);

        // Get the Intent that started this activity and extract the quizzID
        Intent intent = getIntent();

        this.questionID = intent.getIntExtra(ManageQuestionsActivity.QUESTION_ID_MESSAGE, 0);
        this.quizzID = intent.getIntExtra(ManageQuestionsActivity.QUIZZ_ID_MESSAGE, 0);
        this.newQuestion = intent.getBooleanExtra(ManageQuestionsActivity.CONFIGURE_MODE, true);


        //set the adapter for the wrong answers
        final WrongAnswersAdapter wrongAnswersAdapter = new WrongAnswersAdapter(this);
        RecyclerView recyclerView = findViewById(R.id.wrong_answers_recycler_view);
        recyclerView.setAdapter(wrongAnswersAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //set the view model
        mQuestionViewModel = ViewModelProviders.of(this,
                new ViewModelFactory(getApplication(), this.questionID)).get(QuestionViewModel.class);


        mQuestionViewModel.getAnswers().observe(this, new Observer<List<AnswerEntity>>() {
            @Override
            public void onChanged(@Nullable final List<AnswerEntity> answers) {
                // Update the cached copy of the question in the adapter.
                wrongAnswersAdapter.setAnswers(answers);
            }
        });

        //set the view for update
        setViewAccordingToConfigureMode();

        //set the action for the button for adding a wrong answer
        ImageButton add_wrong_answer_button =findViewById(R.id.add_wrong_answer_button);
        add_wrong_answer_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wrongAnswersAdapter.addAnswer(new AnswerEntity());
            }
        });

        //set the validation button onclick action
        Button validation_button =findViewById(R.id.configure_question_button_validation);
        validation_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( isDataValid(wrongAnswersAdapter.getAnswers()) ){
                    validationButtonAction(wrongAnswersAdapter.getAnswers());
                    finish();
                }
            }
        });

    }


    private void setViewAccordingToConfigureMode(){
        if(! newQuestion) {
            EditText QuestionItemView = findViewById(R.id.configure_question_question_edit_text);
            EditText GoodAnswerItemView = findViewById(R.id.configure_question_good_answer_edit_text);

            QuestionEntity q = mQuestionViewModel.getQuestionSync();

            QuestionItemView.setText(q.getQuestion());
            GoodAnswerItemView.setText(q.getGoodAnswer());


            Button validation_button =findViewById(R.id.configure_question_button_validation);
            validation_button.setText(R.string.update_question);
        }
    }

    private void validationButtonAction(List<AnswerEntity> wrong_answers){
        if(newQuestion){
            insertNewQuestion(wrong_answers);
        }
        else {
            updateQuestion(wrong_answers);
        }
    }

    private void insertNewQuestion(List<AnswerEntity> wrong_answers){
        EditText QuestionItemView = findViewById(R.id.configure_question_question_edit_text);
        EditText GoodAnswerItemView = findViewById(R.id.configure_question_good_answer_edit_text);

        //insert question
        String question  = QuestionItemView.getText().toString();
        String goodAnswer = GoodAnswerItemView.getText().toString();

        QuestionEntity newQuestion = new QuestionEntity();
        newQuestion.setQuizzId(quizzID);
        newQuestion.setQuestion(question);
        newQuestion.setGoodAnswer(goodAnswer);
//        QuestionEntity newQuestion = new QuestionEntity(questionID, question, goodAnswer, quizzID);

        questionID = (int) mQuestionViewModel.insert(newQuestion);

        //insert all answers
        for (AnswerEntity answer : wrong_answers) {
            answer.setQuestionId(questionID);
            mQuestionViewModel.insert(answer);
        }

    }

    private void updateQuestion(List<AnswerEntity> wrong_answers){
        EditText QuestionItemView = findViewById(R.id.configure_question_question_edit_text);
        EditText GoodAnswerItemView = findViewById(R.id.configure_question_good_answer_edit_text);

        //udpate question
        String question  = QuestionItemView.getText().toString();
        String goodAnswer = GoodAnswerItemView.getText().toString();

        QuestionEntity newQuestion = new QuestionEntity(questionID, question, goodAnswer, quizzID);


        mQuestionViewModel.update(newQuestion);


        //delete all answers from bdd for the question
        mQuestionViewModel.deleteAllAnswers();


        //insert new answers
        for (AnswerEntity answer : wrong_answers) {
            answer.setQuestionId(questionID);
            mQuestionViewModel.insert(answer);
        }

    }

    private boolean isDataValid (List<AnswerEntity> wrongAnswers){
        EditText QuestionItemView = findViewById(R.id.configure_question_question_edit_text);
        EditText GoodAnswerItemView = findViewById(R.id.configure_question_good_answer_edit_text);

        //check the question
        if(QuestionItemView.getText().toString().equals("")){
            infoMessage(getString(R.string.fill_question));
            return false;
        }
        //checl the good answer
        if(GoodAnswerItemView.getText().toString().equals("")) {
            infoMessage(getString(R.string.fill_good_answer));
            return false;
        }

        //check the wrong answers :
        //check that there is a least one
        if(wrongAnswers.size() < 1){
            infoMessage(getString(R.string.fill_one_wrong_answer));
            return false;
        }

        //check that they are not empty
        for (AnswerEntity a :wrongAnswers) {
            if(a.getText()==null || a.getText().isEmpty()){
                infoMessage(getString(R.string.no_empty_wrong_answer));
                return false;
            }
        }

        return true;
    }


    private void infoMessage (String text){
        Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
    }

}
