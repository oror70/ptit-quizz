package vandroux.aurore.ptit_quizz.ui;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.View;


import android.support.design.widget.FloatingActionButton;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import vandroux.aurore.ptit_quizz.R;
import vandroux.aurore.ptit_quizz.db.entity.QuizzEntity;
import vandroux.aurore.ptit_quizz.viewmodel.QuizzListViewModel;

import static android.text.InputType.TYPE_CLASS_TEXT;

public class ManageQuizzActivity extends AppCompatActivity implements GoToNextActivityInterface {

    public static final String QUIZZ_ID_MESSAGE = "vandroux.aurore.QUIZZ_ID";

    private QuizzListViewModel mQuizzListViewModel;

    private EditText mEditQuizzNameView ;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_quizz);


        //set Adapter
        final QuizzListAdapter adapter = new QuizzListAdapter(this);
        RecyclerView recyclerView = findViewById(R.id.recyclerview_quizz_list);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // Get a new or existing ViewModel from the ViewModelProvider.
        mQuizzListViewModel = ViewModelProviders.of(this).get(QuizzListViewModel.class);

        // Add an observer on the LiveData returned by getQuizzs().
        // The onChanged() method fires when the observed data changes and the activity is
        // in the foreground.
        mQuizzListViewModel.getQuizzs().observe(this, new Observer<List<QuizzEntity>>() {
            @Override
            public void onChanged(@Nullable final List<QuizzEntity> quizzs) {
                // Update the cached copy of the quizzs in the adapter.
                adapter.setQuizzs(quizzs);
            }
        });

        FloatingActionButton fab = findViewById(R.id.fab_manage_quizz);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showNewQuizzDialog();
            }
        });
    }

    public void showNewQuizzDialog() {
        // Create an instance of the dialog fragment and show it

        mEditQuizzNameView = new EditText(this);
        mEditQuizzNameView.setMaxLines(1);
        mEditQuizzNameView.setInputType(TYPE_CLASS_TEXT);
        mEditQuizzNameView.setFilters(new InputFilter[]{new InputFilter.LengthFilter(30)});

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(mEditQuizzNameView) ;
        alertDialogBuilder.setMessage(R.string.enter_new_quizz_name);
        alertDialogBuilder.setPositiveButton(R.string.create_quizz, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                onDialogPositiveClick();
            }
        } );
        alertDialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                onDialogNegativeClick();
            }
        } );
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void onDialogPositiveClick() {
        // User touched the dialog's positive button

        //récuper le nom du nouveau quizz
        if (TextUtils.isEmpty(mEditQuizzNameView.getText())) {
            Toast.makeText(getApplicationContext(), R.string.empty_quizz_name, Toast.LENGTH_SHORT).show();
        } else {
            String quizzName = mEditQuizzNameView.getText().toString();

            //ajouter le nouveau quizz dans la bdd
            QuizzEntity quizz = new QuizzEntity();
            quizz.setName(quizzName);

            mQuizzListViewModel.insert(quizz);
        }
    }

    public void onDialogNegativeClick() {
        // User touched the dialog's negative button
        Toast.makeText(getApplicationContext(), "cancel", Toast.LENGTH_SHORT).show();

    }

    public void goToManageQuestionsActivityOfQuizz(int quizzId){
        Intent intent = new Intent(ManageQuizzActivity.this, ManageQuestionsActivity.class);
        intent.putExtra(QUIZZ_ID_MESSAGE, quizzId);
        startActivity(intent);
    }

    public void goToNextActivity(Object ... params){
        goToManageQuestionsActivityOfQuizz((int) params[0]);
    }
}
