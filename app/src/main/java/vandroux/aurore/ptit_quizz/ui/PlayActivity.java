package vandroux.aurore.ptit_quizz.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import vandroux.aurore.ptit_quizz.R;
import vandroux.aurore.ptit_quizz.db.entity.AnswerEntity;
import vandroux.aurore.ptit_quizz.db.entity.QuestionEntity;
import vandroux.aurore.ptit_quizz.db.entity.QuizzEntity;
import vandroux.aurore.ptit_quizz.viewmodel.QuestionListViewModel;
import vandroux.aurore.ptit_quizz.viewmodel.QuestionViewModel;
import vandroux.aurore.ptit_quizz.viewmodel.ViewModelFactory;

public class PlayActivity extends AppCompatActivity implements GoToNextActivityInterface {

    private int quizzID ;

    private QuestionListViewModel mQuestionListViewModel;
    private QuestionViewModel mQuestionViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);

        // Get the Intent that started this activity and extract the quizzID
        Intent intent = getIntent();

        this.quizzID =  intent.getIntExtra(ManageQuizzActivity.QUIZZ_ID_MESSAGE, 0);

        //set Adapter
        final QuestionAnswersAdapter adapter = new QuestionAnswersAdapter(this);
        RecyclerView recyclerView = findViewById(R.id.play_recyclerview_answer_list);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        //set view models
        mQuestionListViewModel = ViewModelProviders.of(this,
        new ViewModelFactory(getApplication(), this.quizzID)).get(QuestionListViewModel.class);


        //set the title of the activity (the quizz name)
        setQuizzName(this);

        //--------------------------------------------
        //récupérer la liste des question et la mélangée.
        List<QuestionEntity> questions = mQuestionListViewModel.getQuestionsSync();

        Collections.shuffle(questions);


        if(questions.isEmpty()){
            //lol
        }
        else{
            //        pour la première question faire :

            //get the question
            QuestionEntity question = questions.get(0);
            int questionId = question.getId();


            //récupere le view model de la question
            mQuestionViewModel = ViewModelProviders.of(this,
                    new ViewModelFactory(getApplication(), questionId)).get(QuestionViewModel.class);



            //compute the list of answer in random order
            List<String> answers = new ArrayList<>();


            //get the list of wrong answers from db
            List<AnswerEntity> answersEntities = mQuestionViewModel.getAnswersSync();


            //ajouter toutes les mauvaises réponses dans la liste
            for (AnswerEntity a : answersEntities) {
                    answers.add(a.getText());
            }

            //ajouter la bonne réponse à la liste
            String good_answer = question.getGoodAnswer();
            answers.add(good_answer);

            //mélanger la liste
            Collections.shuffle(answers);

            //récupérer l'index de la bonne réponse après mélange
            int good_answer_index = answers.indexOf(good_answer);


            //set the Adapter Data
            adapter.setAnswers(answers, good_answer_index);


            //set the question Text View
            TextView questionTextView = findViewById(R.id.play_question_text);
            questionTextView.setText(question.getQuestion());

            //set the score TextView
            TextView playScoreTextView = findViewById(R.id.play_score);
            playScoreTextView.setText("1/"+questions.size());



        }

    }


    public void setQuizzName(Context context){
        QuizzEntity quizz = mQuestionListViewModel.getQuizzSync();

        //set quizz name
        TextView quizzNameTitle = findViewById(R.id.play_quizz_name);
        quizzNameTitle.setText(quizz.getName());
    }


    public void goToScoreActivityOfQuizz(int quizzId){
        Intent intent = new Intent(PlayActivity.this, ScoreActivity.class);
//        intent.putExtra(QUIZZ_ID_MESSAGE, quizzId);
        startActivity(intent);
    }

    public void goToNextActivity(Object ... params){
        goToScoreActivityOfQuizz((int) params[0]);
    }
}
