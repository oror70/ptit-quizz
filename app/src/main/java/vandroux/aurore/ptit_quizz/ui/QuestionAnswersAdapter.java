package vandroux.aurore.ptit_quizz.ui;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Collections;
import java.util.List;

import vandroux.aurore.ptit_quizz.R;
import vandroux.aurore.ptit_quizz.db.entity.AnswerEntity;

public class QuestionAnswersAdapter extends RecyclerView.Adapter<QuestionAnswersAdapter.AnswersViewHolder>{

    class AnswersViewHolder extends RecyclerView.ViewHolder {
        private final TextView AnswerItemView;
        private final LinearLayout AnswerItemViewBox;

        private AnswersViewHolder(View itemView) {
            super(itemView);
            AnswerItemView = itemView.findViewById(R.id.play_question_answer_TextView);
            AnswerItemViewBox = itemView.findViewById(R.id.play_question_answer_box);
        }
    }

    private final LayoutInflater mInflater;
    private List<String> mAnswers = Collections.emptyList(); // Cached copy of answers
    private int mGoodAnswerPosition = -1 ; // index of the good answers in the list of answers

    private GoToNextActivityInterface goToNextActivityInterface;


    QuestionAnswersAdapter (Context context) {
        mInflater = LayoutInflater.from(context);
    }


    @Override
    public QuestionAnswersAdapter.AnswersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = mInflater.inflate(R.layout.play_question_answer_recyclerview_item, parent, false);

        return new QuestionAnswersAdapter.AnswersViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(QuestionAnswersAdapter.AnswersViewHolder holder, int position) {

        String current = mAnswers.get(position);
        String good_answer = mAnswers.get(mGoodAnswerPosition);

        holder.AnswerItemView.setText(current);

        holder.AnswerItemViewBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //change color

                //go to next question
            }
        });

    }


    void setAnswers(List<String> answers, int good_answer_pos) {
        mAnswers = answers;
        mGoodAnswerPosition = good_answer_pos;
        notifyDataSetChanged();
    }

    List<String> getAnswers(){
        return mAnswers;
    }


    @Override
    public int getItemCount() {
        return mAnswers.size();
    }
}
