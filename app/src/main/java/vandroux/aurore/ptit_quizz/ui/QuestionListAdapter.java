package vandroux.aurore.ptit_quizz.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import vandroux.aurore.ptit_quizz.R;
import vandroux.aurore.ptit_quizz.db.entity.QuestionEntity;

public class QuestionListAdapter  extends RecyclerView.Adapter<QuestionListAdapter.QuestionViewHolder> {

    class QuestionViewHolder extends RecyclerView.ViewHolder {
        private final TextView QuestionItemTextView;
        private final ImageButton buttonPopMenuItemView;

        private QuestionViewHolder(View itemView) {
            super(itemView);
            QuestionItemTextView = itemView.findViewById(R.id.question_name_textView);
            buttonPopMenuItemView = itemView.findViewById(R.id.question_pop_menu_button);
        }
    }

    private final LayoutInflater mInflater;
    private List<QuestionEntity> mQuestions = Collections.emptyList(); // Cached copy of questions
    private GoToNextActivityInterface goToNextActivityInterface;
    private DeleteQuestionInterface deleteQuestionInterface;




    QuestionListAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
        try {
            this.goToNextActivityInterface = ((GoToNextActivityInterface) context);
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement GoToNextActivityInterface.");
        }

        try {
            this.deleteQuestionInterface = ((DeleteQuestionInterface) context);
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement DeleteQuestionInterface.");
        }
    }


    @Override
    public QuestionListAdapter.QuestionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.question_list_recyclerview_item, parent, false);
        return new QuestionListAdapter.QuestionViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(QuestionListAdapter.QuestionViewHolder holder, int position) {
        QuestionEntity current = mQuestions.get(position);
        holder.QuestionItemTextView.setText(current.getQuestion());
        holder.buttonPopMenuItemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu popup = new PopupMenu((Context) goToNextActivityInterface, view);

                // This activity implements OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                     @Override
                     public boolean onMenuItemClick(MenuItem menuItem) {
                         switch (menuItem.getItemId()) {
                             case R.id.configure_question_menu_item:
                                 goToNextActivityInterface.goToNextActivity(current.getId(), false);
                                 return true;


                             case R.id.delete_question_menu_item:
                                 //display an alert dialog
                                 AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder((Context) goToNextActivityInterface);
                                 alertDialogBuilder.setMessage(R.string.dialog_delete_question);
                                 alertDialogBuilder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                                     public void onClick(DialogInterface dialog, int id) {
                                         //delete question from database
                                        deleteQuestionInterface.deleteQuestion(current.getId());
                                     }
                                 } );
                                 alertDialogBuilder.setNegativeButton(R.string.cancel, null);
                                 AlertDialog alertDialog = alertDialogBuilder.create();
                                 alertDialog.show();

                                 return true;

                             default:
                                 return false;
                     }
                 }});

                popup.inflate(R.menu.question_menu_item);
                popup.show();
            }
        });
    }

    void setQuestions(List<QuestionEntity> questions) {
        mQuestions = questions;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mQuestions.size();
    }
}
