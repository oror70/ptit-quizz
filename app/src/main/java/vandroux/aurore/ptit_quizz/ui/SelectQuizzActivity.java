package vandroux.aurore.ptit_quizz.ui;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import vandroux.aurore.ptit_quizz.R;
import vandroux.aurore.ptit_quizz.db.entity.QuizzEntity;
import vandroux.aurore.ptit_quizz.viewmodel.QuizzListViewModel;

public class SelectQuizzActivity extends AppCompatActivity implements GoToNextActivityInterface{

    public static final String QUIZZ_ID_MESSAGE = "vandroux.aurore.QUIZZ_ID";

    private QuizzListViewModel mQuizzListViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_quizz);


        //set Adapter
        final QuizzListAdapter adapter = new QuizzListAdapter(this);
        RecyclerView recyclerView = findViewById(R.id.recyclerview_quizz_list);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // Get a new or existing ViewModel from the ViewModelProvider.
        mQuizzListViewModel = ViewModelProviders.of(this).get(QuizzListViewModel.class);

        // Add an observer on the LiveData returned by getQuizzs().
        // The onChanged() method fires when the observed data changes and the activity is
        // in the foreground.
        mQuizzListViewModel.getQuizzs().observe(this, new Observer<List<QuizzEntity>>() {
            @Override
            public void onChanged(@Nullable final List<QuizzEntity> quizzs) {
                // Update the cached copy of the quizzs in the adapter.
                adapter.setQuizzs(quizzs);
            }
        });
    }

    public void goToPlayActivityOfQuizz(int quizzId){
        Intent intent = new Intent(SelectQuizzActivity.this, PlayActivity.class);
        intent.putExtra(QUIZZ_ID_MESSAGE, quizzId);
        startActivity(intent);
    }

    public void goToNextActivity(Object ... params){
        goToPlayActivityOfQuizz((int) params[0]);
    }
}
