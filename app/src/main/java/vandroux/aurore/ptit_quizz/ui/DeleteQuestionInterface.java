package vandroux.aurore.ptit_quizz.ui;

public interface DeleteQuestionInterface {
    void deleteQuestion(int questionId);
}
