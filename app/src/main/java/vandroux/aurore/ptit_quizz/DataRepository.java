package vandroux.aurore.ptit_quizz;

import java.util.List;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

//import androidx.annotation.Nullable;
//import androidx.lifecycle.LiveData;
//import androidx.lifecycle.MediatorLiveData;
//import androidx.lifecycle.Observer;

import vandroux.aurore.ptit_quizz.db.AppDataBase;
import vandroux.aurore.ptit_quizz.db.dao.AnswerDao;
import vandroux.aurore.ptit_quizz.db.dao.QuestionDao;
import vandroux.aurore.ptit_quizz.db.dao.QuizzDao;
import vandroux.aurore.ptit_quizz.db.entity.AnswerEntity;
import vandroux.aurore.ptit_quizz.db.entity.QuestionEntity;
import vandroux.aurore.ptit_quizz.db.entity.QuizzEntity;
import vandroux.aurore.ptit_quizz.model.Answer;


public class DataRepository {

    private QuizzDao mQuizzDao;
    private QuestionDao mQuestionDao;
    private AnswerDao mAnswerDao;


    public DataRepository(Application application) {
        AppDataBase db = AppDataBase.getInstance(application);

        mQuizzDao = db.quizzDao();
        mQuestionDao = db.questionDao();
        mAnswerDao = db.answerDao();
    }

    //---------------------- QUIZZ

    /**
     * Get the list of quizzs from the database and get notified when the data changes.
     */
    public LiveData<List<QuizzEntity>> getQuizzs() {
        return mQuizzDao.loadAllQuizzs();
    }

    /**
     * Get a quizz from the database and get notified when the data changes.
     */
    public LiveData<QuizzEntity> getQuizz(int quizzID) {
        return mQuizzDao.loadQuizz(quizzID);
    }

    /**
     * Get a question from the database
     */
    public QuizzEntity getQuizzSync(int quizzId) {
        return mQuizzDao.loadQuizzSync(quizzId);
    }


    /**
     * insert a quizz in the dataBase
     */
    public void insertQuizz(QuizzEntity quizz){
        new insertQuizzAsyncTask(mQuizzDao).execute(quizz);
    }

    private static class insertQuizzAsyncTask extends AsyncTask<QuizzEntity, Void, Void> {

        private QuizzDao mAsyncTaskDao;

        insertQuizzAsyncTask(QuizzDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final QuizzEntity... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    public void updateQuizz(QuizzEntity question){
        new updateQuizzAsyncTask(mQuizzDao).execute(question);
    }

    private static class updateQuizzAsyncTask extends AsyncTask<QuizzEntity, Void, Void> {

        private QuizzDao mAsyncTaskDao;

        updateQuizzAsyncTask(QuizzDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final QuizzEntity... params) {
            mAsyncTaskDao.update(params[0]);
            return null;
        }
    }

    public void deleteQuizz(int quizzId){
        new deleteQuizzAsyncTask(mQuizzDao).execute(quizzId);
    }

    private static class deleteQuizzAsyncTask extends AsyncTask<Integer, Void, Void> {

        private QuizzDao mAsyncTaskDao;

        deleteQuizzAsyncTask(QuizzDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Integer... params) {
            mAsyncTaskDao.deleteQuizz(params[0]);
            return null;
        }
    }

    //------------------------- QUESTION

    /**
     * Get the list of questions of a quizz from the database and get notified when the data changes.
     */
    public LiveData<List<QuestionEntity>> getQuestionsOfQuizz(int quizzId) {
        return mQuestionDao.loadQuestionsOfQuizz(quizzId);
    }

    /**
     * Get the list of questions of a quizz from the database
     */
    public List<QuestionEntity> getQuestionsOfQuizzSync(int quizzId) {
        return mQuestionDao.loadQuestionsOfQuizzSync(quizzId);
    }


    /**
     * Get a question from the database and get notified when the data changes.
     */
    public LiveData<QuestionEntity> getQuestion(int questionId) {
        return mQuestionDao.loadQuestion(questionId);
    }

    /**
     * Get a question from the database
     */
    public QuestionEntity getQuestionSync(int questionId) {
        return mQuestionDao.loadQuestionSync(questionId);
    }

    /**
     * insert a question for a quizz in the dataBase
     */
    public long insertQuestion (QuestionEntity question){
//        new insertQuestionAsyncTask(mQuestionDao).execute(question);
        return mQuestionDao.insert(question);
    }

    private static class insertQuestionAsyncTask extends AsyncTask<QuestionEntity, Void, Void> {

        private QuestionDao mAsyncTaskDao;

        insertQuestionAsyncTask(QuestionDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final QuestionEntity... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    public void updateQuestion(QuestionEntity question){
        new updateQuestionAsyncTask(mQuestionDao).execute(question);
    }

    private static class updateQuestionAsyncTask extends AsyncTask<QuestionEntity, Void, Void> {

        private QuestionDao mAsyncTaskDao;

        updateQuestionAsyncTask(QuestionDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final QuestionEntity... params) {
            mAsyncTaskDao.update(params[0]);
            return null;
        }
    }

    public void deleteQuestion(int questionId){
        new deleteQuestionAsyncTask(mQuestionDao).execute(questionId);
    }

    private static class deleteQuestionAsyncTask extends AsyncTask<Integer, Void, Void> {

        private QuestionDao mAsyncTaskDao;

        deleteQuestionAsyncTask(QuestionDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Integer... params) {
            mAsyncTaskDao.deleteQuestion(params[0]);
            return null;
        }
    }

    //--------------------------  ANSWER

    /**
     * Get the list of answers of a question from the database and get notified when the data changes.
     */
    public LiveData<List<AnswerEntity>> getAnswersOfQuestion(int questionId) {
        return mAnswerDao.loadAnswersOfQuestion(questionId);
    }

    public List<AnswerEntity> getAnswersOfQuestionSync(int questionId) {
        return mAnswerDao.loadAnswersOfQuestionSync(questionId);
    }


    /**
     * insert a question for a quizz in the dataBase
     */
    public void insertAnswer (AnswerEntity answer){
        new insertAnswerAsyncTask(mAnswerDao).execute(answer);
    }

    private static class insertAnswerAsyncTask extends AsyncTask<AnswerEntity, Void, Void> {

        private AnswerDao mAsyncTaskDao;

        insertAnswerAsyncTask(AnswerDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final AnswerEntity... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    public void deleteAnswersOfQuestion(int questionId){
        new deleteAnswersOfQuestionAsyncTask(mAnswerDao).execute(questionId);
    }

    private static class deleteAnswersOfQuestionAsyncTask extends AsyncTask<Integer, Void, Void> {

        private AnswerDao mAsyncTaskDao;

        deleteAnswersOfQuestionAsyncTask(AnswerDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Integer... params) {
            mAsyncTaskDao.deleteAnswersOfQuestion(params[0]);
            return null;
        }
    }

}
